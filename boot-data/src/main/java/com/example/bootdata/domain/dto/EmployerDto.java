package com.example.bootdata.domain.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class EmployerDto {
    Long id;
    String name;
    String location;

    protected Date lastModifiedDate;

}
