package com.example.bootdata.domain.dto;

import lombok.*;

import java.util.Date;
import java.util.UUID;
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@ToString
public class AccountDto {
    private Long id;

    private String number = UUID.randomUUID().toString();

    private  String  currency;

    private Double balance = (double) 0;

    protected Date lastModifiedDate;
}
