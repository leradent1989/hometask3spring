package com.example.bootdata.service;




import com.example.bootdata.dao.AccountJpaRepository;
import com.example.bootdata.dao.CustomerJpaRepository;
import com.example.bootdata.domain.hr.Account;
import com.example.bootdata.domain.hr.Customer;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultCustomerService implements CustomerService  {

    private final CustomerJpaRepository customerRepository;

    private final AccountJpaRepository accountRepository;


    @Override
    public List<Customer> findAll(Integer  page, Integer size){

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Customer> departmentPage = customerRepository.findAll(pageable);

        return departmentPage.toList();

    }

    @Override
    public void save(Customer customer) {
        customerRepository.save(customer ) ;
    }

    @Override
    public void update(Customer customer) {
     customerRepository.save(customer) ;
    }

    @Override
    public void delete(Customer customer) {

      customerRepository.deleteById(customer.getId());
    }

    @Override
    public void deleteAll(List<Customer> customers) {
         customerRepository.deleteAll(customers ) ;
    }

    @Override
    public void saveAll(List<Customer> customers) {
           customerRepository.saveAll(customers);
    }
    @Transactional(readOnly = true)
    @Override
    public List<Customer> findAll() {
         return customerRepository.findAll();
    }

    @Override
    public void deleteById(Long id) {

       customerRepository.deleteById(id) ;
    }
    @Transactional(readOnly = true)
    @Override
    public Customer getOne(Long id) {
        return customerRepository.getOne(id);
    }


    public boolean addAccount(Long userId,Account account){

        Customer customer = customerRepository.getOne(userId);


        if(account.getNumber().equals("")){

            Account newAccount =   new Account();
            newAccount.setCurrency(account.getCurrency());
            newAccount.setCustomer(customer);
            accountRepository.save(newAccount);

            List <Account> customerAccounts = customer.getAccounts();
            customerAccounts.add(newAccount) ;
            customer.setAccounts(customerAccounts);

          customerRepository.save(customer) ;

        }else{
            UUID number = UUID.fromString(account.getNumber()) ;
            System.out.println(number);
            Optional<Account> accountOptional = accountRepository.findAll().stream().filter(el->el.getNumber().equals(number.toString())).findAny() ;
            if(accountOptional.isPresent()){
                Account accountFromOptional= accountOptional.get();
                Account   account1 =accountRepository.getOne(  accountFromOptional.getId());
                account1.setCustomer(customer);
                List <Account> customerAccounts = customer.getAccounts();
                customerAccounts.add(account1) ;
                customer.setAccounts(customerAccounts);
                System.out.println(customer.getAccounts());
                customerRepository.save(customer);

                accountRepository.save(account1) ;}else{
                return false;
            }
        }
        return  true;



    }


}
