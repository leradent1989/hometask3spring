package com.example.bootdata.service.dtomapper;

import com.example.bootdata.domain.dto.EmployerDto;
import com.example.bootdata.domain.hr.Employer;
import org.springframework.stereotype.Service;


@Service
public class EmployerDtoMapper extends DtoMapperFacade<Employer, EmployerDto>{
    public EmployerDtoMapper (){super(Employer.class , EmployerDto.class); }
}
