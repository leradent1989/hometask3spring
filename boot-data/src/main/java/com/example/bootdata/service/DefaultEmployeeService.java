package com.example.bootdata.service;



import com.example.bootdata.dao.EmployerJpaRepository;
import com.example.bootdata.domain.hr.Employer;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Service
@Transactional
@RequiredArgsConstructor
public class DefaultEmployeeService implements EmployerService {
    private final EmployerJpaRepository employeeRepository;

    public List<Employer> findAll(Integer  page, Integer size){

        Sort sort =  Sort.by(new Sort.Order(Sort.Direction.ASC,"id"));
        Pageable pageable = PageRequest.of(page,size,sort);
        Page<Employer> departmentPage = employeeRepository.findAll(pageable);

        return departmentPage.toList();

    }


    @Override
    @Transactional(readOnly = true, rollbackFor = NoSuchElementException.class, timeout = 1000)
    public List<Employer> getAll() {
        return employeeRepository.findAll();
    }
    @Override
    public void create(Employer employee) {
        employeeRepository.save(employee);
    }

    @Override
    @Transactional(readOnly = true)
    public Employer getById(Long userId) {
        return employeeRepository.getById(userId);
    }

    @Override
    public void update(Employer employee) {
        employeeRepository.save(employee);

    }

    public void deleteById(Long id) {
        employeeRepository.deleteById(id);

    }
    @Override
    public void delete(Employer employee) {
        employeeRepository.delete(employee);

    }

}
