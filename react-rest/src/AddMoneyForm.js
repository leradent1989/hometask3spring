import React,{useRef} from "react";
import axios from "axios";



const AddMoneyForm = () => {

    const idRef = useRef(null);
    const amountRef = useRef(null);
    const handleSubmit = (e) => {
        e.preventDefault();


        fetch(`http://localhost:9000/accounts/balance/${idRef.current.value}/${amountRef.current.value}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            }

        })

    }

    return (
        <form onSubmit={(e) => handleSubmit(e)}>


            <input
                type="text"
                name="id"
                placeholder="Amount"
                ref={idRef}
            /><br/>

            <input
                type="text"
                name="amount"
                placeholder="AccountId"
                ref={amountRef}
            /><br/>


            <button type="submit">Add money</button>
        </form>
    )
}

export default AddMoneyForm;