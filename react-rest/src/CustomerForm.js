import React,{useRef} from "react";


const CustomerForm = () => {

    const nameRef = useRef(null);
    const emailRef = useRef(null);
    const ageRef = useRef(null);
    const passwordRef = useRef(null);
    const telRef = useRef(null);

    const handleSubmit = (e) => {
        e.preventDefault();

        const body = {
            name: nameRef.current.value,
            email: emailRef.current.value,
            age: ageRef.current.value,
            password:passwordRef.current.value,
            telNumber:telRef.current.value
        }

        console.log(body);
        fetch(`http://localhost:9000/customers`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ id:1,name:body.name,email:body.email,password:body.password,telNumber:body.telNumber,age:body.age })
        })

    }


    return (
        <form onSubmit={(e) => handleSubmit(e)}>


            <input
                type="text"
                name="name"
                placeholder="Name"
                ref={nameRef}
            /><br/>

            <input
                type="text"
                name="email"
                placeholder="Email"
                ref={emailRef}
            /><br/>

            <input
                type="text"
                name="age"
                placeholder="Age"
                ref={ageRef}
            /><br/>
            <input
                type="text"
                name="telNumber"
                placeholder="Mobile number"
                ref={telRef}
            /><br/>
            <input
                type="text"
                name="password"
                placeholder="Password"
                ref={passwordRef}
            /><br/>
            <button type="submit">Add new customer</button>
        </form>
    )
}

export default CustomerForm;
